package web.elements;

import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Header {
    @FindBy(css = "#header .region a")
    private WebElement regionButton;

    private final WebDriver webDriver;
    private final WebDriverWait webDriverWait;

    public Header(WebDriver webDriver) {
        this.webDriver = webDriver;
        webDriverWait = new WebDriverWait(webDriver, 30);

        PageFactory.initElements(webDriver, this);
    }

    @Step("Клик на кнопку выбора региона - открытие формы выбора региона")
    public RegionForm clickOnRegionButton() {
        webDriverWait.until(ExpectedConditions.elementToBeClickable(regionButton)).click();
        return new RegionForm(webDriver);
    }

    @Step("[ОР] Отображается текущий регион: \"{expectedText}\"")
    public void currentRegionShouldBe(String expectedText) {
        webDriverWait.until(ExpectedConditions.textToBePresentInElement(regionButton, expectedText));
    }
}
