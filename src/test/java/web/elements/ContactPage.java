package web.elements;

import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ContactPage {
    private static final String HR_PHONE_LOCATOR =
            "//table[@class='contact-page-table']//tr[child::td[text()=' Отдел подбора персонала ']]/td[2]/div[contains(text(), '+7')]";

    @FindBy(xpath = HR_PHONE_LOCATOR)
    private WebElement hrPhone;

    public ContactPage(WebDriver webDriver) {

        if (!webDriver.getTitle().contains("Контакты интернет-магазина ВсеИнструменты.ру")) {
            throw new IllegalStateException("This is not contact page");
        }

        PageFactory.initElements(webDriver, this);
    }

    @Step("Найти телефон отдела подбора персонала")
    public String getHrPhone() {
        return hrPhone.getText();
    }
}
