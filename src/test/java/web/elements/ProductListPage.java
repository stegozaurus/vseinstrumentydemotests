package web.elements;

import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import web.WebAsserts;

import java.util.List;

public class ProductListPage {
    private final static String ADD_TO_BASKET_LOCATOR = ".addItemToBasket";
    private final static String BEST_PRICE_COLLECTION_LOCATOR =
            "//div[@class='tile-box product'][child::div//a//span[text()='Лучшая цена']]";
    private final WebDriver webDriver;
    private final WebAsserts webAsserts;

    @FindBy(xpath = BEST_PRICE_COLLECTION_LOCATOR)
    private List<WebElement> bestPriceCollection;

    public ProductListPage(WebDriver webDriver) {
        this.webDriver = webDriver;
        webAsserts = new WebAsserts(webDriver);

        PageFactory.initElements(webDriver, this);
    }

    @Step("Выбрать карточку товара для дальнейшей работы")
    public TileProduct chooseBestPriceTile(int index) {
        webAsserts.collectionExists(bestPriceCollection);
        WebElement webElement = bestPriceCollection.get(index);

        return new TileProduct(webElement, webDriver);
    }

    public static class TileProduct {
        private final WebElement tileProduct;
        private final WebDriverWait webDriverWait;

        public TileProduct(WebElement tileProduct, WebDriver webDriver) {
            this.tileProduct = tileProduct;
            webDriverWait = new WebDriverWait(webDriver, 30);
        }

        @Step("Узнать цену товара")
        public String getPrice() {
            String fullString = tileProduct.findElement(By.className("price-actual")).getText();
            return fullString.replace(" р.", "");
        }

        @Step("Узнать название товара")
        public String getProductName() {
            return tileProduct.findElement(By.cssSelector(".product-name a span")).getText();
        }

        @Step("Узнать ссылку на товар")
        public String getProductLink() {
            return tileProduct.findElement(By.cssSelector(".product-name a")).getAttribute("href");
        }

        @Step("Узнать код товара")
        public String getProductCode() {
            return tileProduct.findElement(By.cssSelector(".code span")).getText();
        }

        @Step("Клик на кнопку \"В корзину\"")
        public void clickOnToBasketButton() {
            WebElement addToBasketButton = tileProduct.findElement(By.cssSelector(ADD_TO_BASKET_LOCATOR));
            webDriverWait.until(ExpectedConditions.elementToBeClickable(addToBasketButton)).click();
        }
    }
}
