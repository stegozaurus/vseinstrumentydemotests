package web.elements;

import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import web.WebAsserts;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class RegionForm {
    private final static String BASE_LOCATOR = "#popUpWindow1";
    private final static String DELIVERY_COURIER_LOCATOR = ".deliveryCourierConteiner";

    private final WebDriverWait webDriverWait;

    @FindBy(css = BASE_LOCATOR + " .form-head-name")
    private WebElement formTitle;

    @FindBy(css = BASE_LOCATOR + " form#check[action='/represent/change/']")
    private WebElement regionForm;

    private List<WebElement> deliveryCourierCollection;
    private final WebAsserts webAsserts;

    public RegionForm(WebDriver webDriver) {
        webDriverWait = new WebDriverWait(webDriver, 30);
        webAsserts = new WebAsserts(webDriver);

        PageFactory.initElements(webDriver, this);
    }

    @Step("Найти заголовок формы")
    public String getFormTitle() {
        return formTitle.getText();
    }

    @Step("Найти коллекцию всех регионов в форме выбора региона")
    public RegionForm initDeliveryCourierCollection() {
        deliveryCourierCollection = regionForm.findElements(By.cssSelector(DELIVERY_COURIER_LOCATOR));
        webAsserts.collectionExists(deliveryCourierCollection);

        return this;
    }

    @Step("Исключить регионы с жирным шрифтом")
    public RegionForm excludeBoldRegion() {
        if (deliveryCourierCollection == null) {
            initDeliveryCourierCollection();
        }

        deliveryCourierCollection = deliveryCourierCollection.stream()
                .filter(delivery -> hasCssProp("font-weight", "400", delivery))
                .collect(Collectors.toList());

        webAsserts.collectionExists(deliveryCourierCollection);

        return this;
    }

    @Step("Кликнуть на случайный регион")
    public String clickOnRandomRegion() {
        if (deliveryCourierCollection == null) {
            initDeliveryCourierCollection();
        }

        Collections.shuffle(deliveryCourierCollection);
        WebElement firstAfterShuffle = deliveryCourierCollection.get(0);

        String regionText = firstAfterShuffle.getText();
        clickOn(regionText, firstAfterShuffle);

        return regionText;
    }

    @Step("Клик на элемент с текстом \"{elText}\"")
    private void clickOn(String elText, WebElement element) {
        webDriverWait.until(ExpectedConditions.elementToBeClickable(element)).click();
    }

    private boolean hasCssProp(String cssName, String cssValue, WebElement webElement) {
        String actCssValue = webElement.findElement(By.cssSelector("a")).getCssValue(cssName);
        return actCssValue.equals(cssValue);
    }
}
