package web.elements;

import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import web.WebAsserts;

import java.util.List;

public class BasketPage {
    private final static String PRODUCT_LINK_LOCATOR = ".cart-goods .-name a";
    private final static String PRODUCT_CODE_LOCATOR = ".//div[@class='flex-column main']/small";

    @FindBy(xpath = "//div[@class='row -header']/following-sibling::div")
    private List<WebElement> productCardCollection;

    @FindBy(className = "cart-total")
    private WebElement cardTotal;

    private final WebAsserts webAsserts;

    public BasketPage(WebDriver webDriver) {
        WebDriverWait webDriverWait = new WebDriverWait(webDriver, 30);
        webDriverWait.until(ExpectedConditions.visibilityOf(webDriver.findElement(By.cssSelector(PRODUCT_LINK_LOCATOR))));
        webAsserts = new WebAsserts(webDriver);

        PageFactory.initElements(webDriver, this);
    }

    @Step("Выбрать к корзине карточку товара для дальнейшей работы")
    public ProductCard chooseCard(int index) {
        webAsserts.collectionExists(productCardCollection);

        WebElement webElement = productCardCollection.get(index);

        return new ProductCard(webElement);
    }

    @Step("Узнать итоговую стоимость")
    public String getTotalPrice() {
        String fullString =  cardTotal.findElement(By.className("-price")).getText();
        return fullString.replace(" р.", "");
    }

    public static class ProductCard {
        private final WebElement productCard;

        public ProductCard(WebElement productCard) {
            this.productCard = productCard;
        }

        @Step("Узнать ссылку на товар")
        public String getProductLink() {
            return productCard.findElement(By.cssSelector(PRODUCT_LINK_LOCATOR)).getAttribute("href");
        }

        @Step("Узнать стоимость")
        public String getPrice() {
            String fullString = productCard.findElement(By.xpath(".//div[@class='col -sum']")).getText();
            return fullString.replace(" р.", "");
        }

        @Step("Узнать название товара")
        public String getProductName() {
            return productCard.findElement(By.cssSelector(PRODUCT_LINK_LOCATOR)).getText();
        }

        @Step("Узнать код товара")
        public String getProductCode() {
            String productCodeFullText = productCard.findElement(By.xpath(PRODUCT_CODE_LOCATOR)).getText();
            return productCodeFullText.substring(productCodeFullText.indexOf(":") + 1).trim();
        }
    }
}
