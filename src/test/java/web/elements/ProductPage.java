package web.elements;

import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ProductPage {
    private final static String ADD_TO_BASKET_LOCATOR = ".addItemToBasket";
    private final WebDriver webDriver;

    @FindBy(id = "card-h1-reload-new")
    private WebElement productName;

    @FindBy(css = "#aboveImageBlock .codeToOrder")
    private WebElement productCode;

    @FindBy(id = "card-resale-sale")
    private WebElement cardResail;

    public ProductPage(WebDriver webDriver) {
        this.webDriver = webDriver;
        PageFactory.initElements(webDriver, this);
    }

    @Step("Узнать название товара")
    public String getProductName() {
        return productName.getText();
    }

    @Step("Узнать код товара")
    public String getProductCode() {
        return productCode.getText();
    }

    public CardResail getCardResail() {
        if (!cardResail.isDisplayed()) {
            throw new NoSuchElementException("Element not found");
        }
        return new CardResail(cardResail, webDriver);
    }

    public static class CardResail {
        private final WebElement cardResail;
        private final WebDriverWait webDriverWait;

        public CardResail(WebElement cardResail, WebDriver webDriver) {
            this.cardResail = cardResail;
            webDriverWait = new WebDriverWait(webDriver, 30);
        }

        @Step("Узнать цену товара")
        public String getPrice() {
            return cardResail.findElement(By.className("price-value")).getText();
        }

        @Step("Клик на кнопку \"В корзину\"")
        public void clickOnToBasketButton() {
            WebElement addToBasketButton = cardResail.findElement(By.cssSelector(ADD_TO_BASKET_LOCATOR));
            webDriverWait.until(ExpectedConditions.elementToBeClickable(addToBasketButton)).click();
        }
    }
}
