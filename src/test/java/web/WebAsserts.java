package web;

import io.qameta.allure.Step;
import org.junit.jupiter.api.Assertions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class WebAsserts {
    private static final String MODAL_HEADER = ".modals-content .header";

    private WebDriver webDriver;
    private WebDriverWait webDriverWait;

    public WebAsserts(WebDriver webDriver) {
        this.webDriver = webDriver;
        webDriverWait = new WebDriverWait(webDriver, 30);
    }

    @Step("[ОР] Открыто модальное окно с заголовком: \"{expectedTitle}\"")
    public void presentedModalWithHeader(String expectedTitle) {
        WebElement webElement = webDriver.findElement(By.cssSelector(MODAL_HEADER));
        String actualText = webDriverWait.until(ExpectedConditions.visibilityOf(webElement)).getText();
        if (!expectedTitle.equals(actualText)) {
            Assertions.fail(String.format("Modal \"%s\" was not opened", expectedTitle));
        }
    }

    @Step("[ОР] Ненулевое кол-во элементов")
    public void collectionExists(List<WebElement> collection) {
        Assertions.assertNotEquals(0, collection.size(), "Collection is empty");
    }
}
