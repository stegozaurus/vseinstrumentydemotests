import io.qameta.allure.Allure;
import io.qameta.allure.Step;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

public class BaseTest {
    private final static String DRIVER_PATH = "/home/work/chromedriver_linux64/chromedriver";
    protected final static String BASE_URL = "https://www.vseinstrumenti.ru/";

    protected static Logger logger = Logger.getLogger(BaseTest.class.getName());

    protected WebDriver webDriver;

    @BeforeAll
    static void beforeAll() {
        System.setProperty("webdriver.chrome.driver", DRIVER_PATH);
    }

    @BeforeEach
    void setUp() {
        Allure.step("Старт и открытие главной страницы");
        webDriver = new ChromeDriver();
        webDriver.manage().window().setSize(new Dimension(1920, 1080));
        webDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        webDriver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
        webDriver.manage().timeouts().setScriptTimeout(5, TimeUnit.SECONDS);
        webDriver.get(BASE_URL);
    }

    @AfterEach
    void tearDown() {
        Allure.step("Закрытие браузера и завершение");
        if (webDriver != null) {
            webDriver.quit();
            logger.log(Level.INFO, "Web driver quited");
        }
    }

    @Step("Открыть страницу: {relPagePath}")
    protected void openPage(String relPagePath) {
        webDriver.get(BASE_URL + relPagePath);
    }
}
