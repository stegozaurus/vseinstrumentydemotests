package utils;

import io.qameta.allure.Step;
import org.junit.jupiter.api.Assertions;

import java.util.logging.Level;
import java.util.logging.Logger;

public class AssertionThat {
    private static final Logger logger = Logger.getLogger(AssertionThat.class.getName());

    @Step("[ОР] Получена строка: {expected}")
    public static void stringToEqual(String expected, String actual) {
        logger.log(Level.INFO, String.format("Assertion that string \"%s\" is equal \"%s\"", actual, expected));

        Assertions.assertEquals(expected, actual, String.format("String \"%s\" is not equal \"%s\"", actual, expected));

        logger.log(Level.INFO, "Passed");
    }
}
