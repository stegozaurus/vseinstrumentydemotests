import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import utils.AllureHelper;
import utils.AssertionThat;
import web.WebAsserts;
import web.elements.*;

@DisplayName("Тествое задание")
public class DemoTest extends BaseTest {
    @DisplayName("Автотест взятия заголовка страницы")
    @Test
    void test01() {
        String expectedTitle = "Интернет-магазин ВсеИнструменты.ру - электроинструмент и оборудование: климатическое, садовое, клининговое, автогаражное";
        String actualTitle = webDriver.getTitle();

        AssertionThat.stringToEqual(expectedTitle, actualTitle);
    }

    @DisplayName("Автотест нажатия на ссылку вызова формы выбора города")
    @Test
    void test02() {
        String expectedFormTitle = "Выберите свой город";
        Header header = new Header(webDriver);
        RegionForm regionForm = header.clickOnRegionButton();
        String actualTitle = regionForm.getFormTitle();

        AssertionThat.stringToEqual(expectedFormTitle, actualTitle);
    }

    @DisplayName("Нажатие кнопки \"В корзину\" - по распродажной цене")
    @Test
    void test03() {
        final String productPath = "ruchnoy-instrument/otvertki/dinamometricheskie/norgau/20-400snm-052101004/";
        openPage(productPath);

        ProductPage productPage = new ProductPage(webDriver);
        String productName = productPage.getProductName();
        String productCode = productPage.getProductCode();
        String productPrice = productPage.getCardResail().getPrice();

        productPage.getCardResail().clickOnToBasketButton();
        checkAddingToBasket(productName, productCode, BASE_URL + productPath, productPrice);
    }

    @DisplayName("Выбора случайного региона, в котором возможна курьерская доставка")
    @Test
    void test04() {
        Header header = new Header(webDriver);
        RegionForm regionForm = header.clickOnRegionButton();
        String regionText = regionForm.initDeliveryCourierCollection().excludeBoldRegion().clickOnRandomRegion();
        header.currentRegionShouldBe(regionText);
    }

    @DisplayName("Нажатие кнопки \"В корзину\" для случайного товара с признаком \"Лучшая цена\"")
    @Test
    void test05() {
        openPage("instrument/dreli/");

        ProductListPage productListPage = new ProductListPage(webDriver);

        ProductListPage.TileProduct tileProduct = productListPage.chooseBestPriceTile(0);
        String productName = tileProduct.getProductName();
        String productCode = tileProduct.getProductCode();
        String productLink = tileProduct.getProductLink();
        String productPrice = tileProduct.getPrice();

        tileProduct.clickOnToBasketButton();

        checkAddingToBasket(productName, productCode, productLink, productPrice);
    }

    @DisplayName("Узнать телефон отдела подбора персонала со страницы \"Контакты\"")
    @Test
    void test06() {
        openPage("contacts/1.html");
        ContactPage contactPage = new ContactPage(webDriver);
        String hrPhone = contactPage.getHrPhone();
        AllureHelper.attachText("Телефон отдела подбора персонала", hrPhone);
    }

    private void checkAddingToBasket(String expName, String expCode, String expLink, String expPrice) {
        WebAsserts webAsserts = new WebAsserts(webDriver);
        webAsserts.presentedModalWithHeader("Товар добавлен в корзину");

        openPage("pcabinet/?block=myCart");

        BasketPage basketPage = new BasketPage(webDriver);
        BasketPage.ProductCard basketProductCard =  basketPage.chooseCard(0);

        String basketName = basketProductCard.getProductName();
        AssertionThat.stringToEqual(expName, basketName);

        String basketProductCode = basketProductCard.getProductCode();
        AssertionThat.stringToEqual(expCode, basketProductCode);

        String basketLink = basketProductCard.getProductLink();
        AssertionThat.stringToEqual(expLink, basketLink);

        String basketPrice = basketProductCard.getPrice();
        AssertionThat.stringToEqual(expPrice, basketPrice);

        String basketTotalPrice = basketPage.getTotalPrice();
        AssertionThat.stringToEqual(expPrice, basketTotalPrice);
    }
}
